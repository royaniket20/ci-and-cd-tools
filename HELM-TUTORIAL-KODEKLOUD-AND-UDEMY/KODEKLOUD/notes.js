// helm --help

// helm repo --help

// To Install a Wordpress Helm chart to my kubernetes Cluster
// helm repo add bitnami https://charts.bitnami.com/bitnami
// helm install my-release bitnami/wordpress

//When you want to search on Hub level
//helm search hub wordpress
//When you want to search on Specific Repo  - This will only Search on Spicific Repo already Configured
//helm search repo wordpress

//helm list - list all releases 
//helm repo list
//helm repo update - Update the Lateset Meta Info to your Local Machine 


//IMPORTANT HOW TO ACCESS A LOAD BALANCER TYPE SERVICE IN MINIKUBE FROM LOCAL  HOST MACHINE 

//minikube tunnel - oN A DIFFERENT TERMINAL 
//minikube service my-first-wordpress-release --url   - WILL GIVE THE URL 

//helm install pull the image and immediately Install it to Kubernetes Cluster 
//If few Attributes need to change you can use Command Line argument like below 
// helm install --set wordpressBlogName= "Aniket's Blog" my-first-wordpress-release bitnami/wordpress
//If many Values Need to be Updated then Use a Custom-values.yml and then do below 
//helm upgrade --values custom-values.yml my-second-wordpress-release bitnami/wordpress

//You can Pull the Fuiles and Modify the Data 
//helm pull bitnami/wordpress
//Or
//helm pull --untar bitnami/wordpress
//helm upgrade  my-second-wordpress-release ./wordpress

//Remove Repository --- 
//helm repo remove hashicorp
//Helm Update in action
//helm upgrade dazzling-web bitnami/nginx --version 12

/**
 * This command upgrades a release to a new version of a chart.

The upgrade arguments must be a release and chart. The chart argument can be either: a chart reference('example/mariadb'), a path to a chart directory, a packaged chart, or a fully qualified URL. For chart references, the latest version will be specified unless the '--version' flag is set.

To override values in a chart, use either the '--values' flag and pass in a file or use the '--set' flag and pass configuration from the command line, to force string values, use '--set-string'. You can use '--set-file' to set individual values from a file when the value itself is too long for the command line or is dynamically generated.

You can specify the '--values'/'-f' flag multiple times. The priority will be given to the last (right-most) file specified. For example, if both myvalues.yaml and override.yaml contained a key called 'Test', the value set in override.yaml would take precedence:

$ helm upgrade -f myvalues.yaml -f override.yaml redis ./redis
You can specify the '--set' flag multiple times. The priority will be given to the last (right-most) set specified. For example, if both 'bar' and 'newbar' values are set for a key called 'foo', the 'newbar' value would take precedence:

$ helm upgrade --set foo=bar --set foo=newbar redis ./redis


 helm history dazzling-web
 
 
 aniket@Sriparna:/mnt/c/Users/royan/ci-and-cd-tools$ gpg --list-keys
gpg: checking the trustdb
gpg: marginals needed: 3  completes needed: 1  trust model: pgp
gpg: depth: 0  valid:   1  signed:   0  trust: 0-, 0q, 0n, 0m, 0f, 1u
gpg: next trustdb check due at 2024-06-29
/home/aniket/.gnupg/pubring.kbx
-------------------------------
pub   rsa3072 2022-06-30 [SC] [expires: 2024-06-29]
      F6F9EF2495D9FC7EE082D967BB51202EC76A58F2
uid           [ultimate] Aniket Roy
sub   rsa3072 2022-06-30 [E]



sha256sum my-first-helm-chart-0.1.0.tgz

526ed8d5c180e9e4e997e59f2cda48ae5db6f6934b7c027e5f4415004de755b1  my-first-helm-chart-0.1.0.tgz


cat my-first-helm-chart-0.1.0.tgz.prov

-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

apiVersion: v2
appVersion: 1.16.0
description: A Helm chart for Kubernetes - Created By Anilet
icon: https://www.flaticon.com/free-sticker/baby-shower_4891213?related_id=4891213
maintainers:
- - email: royaniket20@gmail.com
  name: Aniket Roy
name: my-first-helm-chart
type: application
version: 0.1.0

...
files:
  my-first-helm-chart-0.1.0.tgz: sha256:526ed8d5c180e9e4e997e59f2cda48ae5db6f6934b7c027e5f4415004de755b1
-----BEGIN PGP SIGNATURE-----

wsDcBAEBCgAQBQJivdSPCRC7USAux2pY8gAAXrcMAGsHQX9HIDJcNtGJs31i5TXz
veyv+6oalV3lzfNGil0xBYJSM0hlgYo7CvjzqWbYk8wZ4PiwDV4Be6ixBrf66Rpf
oH9VZuWa1LyPRo/fXn38JQaTXgE+YhSatoM8bCqJI7UeRRRwcBB72ZM0AOmFXdho
/lfKriYaQBqQpk9PX3AcsdQ/0+to0DRA2GM05PfwBUbKIhlbGYxLJO124YDreD8G
EPknoO7LaqI08PJ2bUupjGl849f7J/Fd5nfUNP2WzbGurhzrcocbAhGcL4s00hmB
t9ll6RUw/HD/pFr4T1gFyVuPwL4z53qWYYODm1t69sDrqb1ntXqpdla4eYwgpomU
iLuHkUrO7mY5nGfuZJ9zlRXa3x1MvfQ778Bz1RATE77yI/A1JKvd1Pi9w07q28ld
bxfEcS3JR3HW/V9aGgUgCv+ohuBnYXnTglwxJKmPSqmlTWwXvO+BMk6PBpjqMSvN
L3OcLrJQVWD4+vg6BdYZfbwnTthFdf2nNu9qiJ2s6A==
=IMX6
-----END PGP SIGNATURE-----


Verification of Chart 
gpg --export "Aniket Roy" > my-public-key

helm verify --keyring .\my-public-key .\my-first-helm-chart-0.1.0.tgz
Signed by: Aniket Roy
Using Key With Fingerprint: F6F9EF2495D9FC7EE082D967BB51202EC76A58F2
Chart Hash Verified: sha256:526ed8d5c180e9e4e997e59f2cda48ae5db6f6934b7c027e5f4415004de755b1
 


Create New Folder 

Put tgz , pov files in that Dir 
Go to that Dir 
 helm repo index . --url https://example.com/charts
 Url is the url where Chat will be available to download 

 You can then Upload the Files in Google Drive , S3 , Github page etc 

helm repo add my-personal-repo https://example.com/charts

Then you can download and Verify and Install chart

 */